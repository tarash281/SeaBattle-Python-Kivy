# SeaBattle-Python-Kivy

How to lauch game:
1. Clone the project to your local computer;
2. Download Kivy and all required frameworks & libraries;
3. To launch game, get in file "main.py" and launch there.

Directories:
1. main.py - file for launching game;
2. highscores - file for containing highscores info;
3. soundfile - directory with all game audio;
4. assets - directory with fonts and ingame images; 
5. app - main directory;
  5.1 app/color.py - contains colors info;
  5.2 app/game.py - contains all necessary classes like Player, Ship and AI;
  5.3 app/screen - directory with kv dir and .py files for showing image;
    5.3.1 app/screen/kv - directory with all .kv files for all scenes layout;
    5.3.2 app/screen/{all .py files} - files for each scene interface.
